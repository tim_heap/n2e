n2e - Internationalized name handling
=====================================

Names are complicated beasts. Not everyone culture follows the western `$firstName $lastName` pattern.
Not all people have a last name, or maybe they have a couple of family names. What about middle names?
Titles, honourifics, initials and everything else only serve to complicate matters. We have not even
started on the different ways of using those names, depending upon the formality of the situation!
Quite simply, requiring and only supporting people with `$firstName $lastName` is naive, culturally
insensitive, and - for a large portion of the worlds population - just plain old incorrect.

`n2e` is here to help! When signing up to your application, new users can enter their name how ever
they please. The Western style of `$firstName $lastName` is in there, but so is the traditional Chinese 
`$familyName $generationName $givenName`, the Spanish `$givenName $mothersLastName $fathersLastName`,
and the Thai practice of using a nickname for many informal situations.

Users will enter their name in which ever manner is most appropriate for them, which will be stored
by the application. Later on, when you need to address your users, you can obtain their name formatted
for the situation you are using it. A users name can be printed formally, informally, in a friendly
manner, or in a way suitable for sorting in a list.

Different formatting options
----------------------------

How you address a person varies greatly according to the situation, and the culture the person is from.
While it would be fine to address me as "Tim" in an informal setting - even if you did not know me -
doing this in more formal cultures would be considered the height of rudeness. As such, `n2e` provides
a common interface for obtaining the correct version of a name depending upon the situation and the
culture.

### Full name

What it says on the tin. The users full, unabridged name, with all honourifics, titles, etc.

### Legal name

The users name, as it should be rendered on legal documents. Often (but not always) the same as someones
full name.

### Formal name

The users name, as it should be used in a formal setting. Used for introducing people to one another,
or for other uses where a degree of familiarity is not warranted.

### Informal name

The users name, as it should be used in an informal settings. Greeting a returning user to your site,
referring to a friend of the user, or other places where there is a degree of familiarity.

### Friendly name

The users name, as it should be used when referring to good friends. Your application should probably
not use this in communications with the user (such as email), as this implies your application is a
good friend of the user.

### Sortable name

The users name, suitable for sorting. Not all cultures sort names based off the same criterion. While
Western names are sorted by their family names, while Thai and Icelandic names are sorted by their 
given name. Sorting names from multiple cultures in to the same list could lead to strange results.
This requires some more thought to be applied.

Guessing what type of name a user has
-------------------------------------

`n2e` can attempt to guess the correct form of name for a visitor, based upon their location, language,
and any other relevant information. This guess will not always be correct (e.g., international visitors)
but will hopefully help out many users.

If the guess is incorrect, there should be the option of changing the format of the name.

Implemented name formats
------------------------

The currently implemented name formats is:

* Western `$firstName $lastName`, with honourifics, titles and initials as appropriate.
* Completely custom names, where a user fills in their name for each of the above formats.

Please submit a patch if you want to add support for more formats!

What is with the name `n2e`?
-------------------

It is a pun, based upon i18n (internationalisation) and l10n (localisation).

Further reading
---------------

This project was inspired by the W3C write up on [Personal names around the world][w3c]. It is a
fascinating read.

[w3c]: http://www.w3.org/International/questions/qa-personal-names
